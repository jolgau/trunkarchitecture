import { Game } from './classes/Game';

window.addEventListener('DOMContentLoaded', () => {
    const game = new Game('canvas');
    game.initialize();
})