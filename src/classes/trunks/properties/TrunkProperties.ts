export class TrunkProperties {
    private _type: string;
    private _height: number;
    private _diameter: number;
    private _tessellation: number;

    constructor(type: string, height: number, diameter: number, tessellation: number) {
        this._type = type;
        this._height = height;
        this._diameter = diameter;
        this._tessellation = tessellation;
    }

    public get type(): string {
        return this._type;
    }

    public set type(value: string) {
        this._type = value;
    }

    public get height(): number {
        return this._height;
    }

    public set height(value: number) {
        this._height = value;
    }

    public get diameter(): number {
        return this._diameter;
    }

    public set diameter(value: number) {
        this._diameter = value;
    }

    public get tessellation(): number {
        return this._tessellation;
    }

    public set tessellation(value: number) {
        this._tessellation = value;
    }
}