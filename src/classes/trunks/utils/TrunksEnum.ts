export enum TrunksEnum {
    MAPLE = 'maple',
    BIRCH = 'birch',
    PINE = 'pine',
    BAMBOO = 'bamboo'
}