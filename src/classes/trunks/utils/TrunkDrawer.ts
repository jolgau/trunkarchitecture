import { Mesh } from "@babylonjs/core/Meshes/mesh";
import { MeshBuilder } from "@babylonjs/core/Meshes/meshBuilder";
import { TrunkProperties } from "../properties/TrunkProperties";
import { Vector4, Vector3 } from "@babylonjs/core/Maths/math";
import { SceneInstance } from "../../utils/SceneInstance";

export abstract class TrunkDrawer {

    public static drawCylindricalTrunk(trunk: TrunkProperties): Mesh {
        const trunkMesh = MeshBuilder.CreateCylinder(trunk.type, {
            height: trunk.height, diameterTop: trunk.diameter,
            diameterBottom: trunk.diameter, tessellation: trunk.tessellation, faceUV: this.splitCylinderUVFacesForTextures()
        }, SceneInstance.scene);
        return trunkMesh;
    }

    public static drawTubeTrunk(trunk: TrunkProperties): Mesh {
        var tubePath = [
            new Vector3(0, -(trunk.height / 2), 0),
            new Vector3(0, trunk.height / 2, 0),
        ];

        const tubeTrunk = MeshBuilder.CreateTube(trunk.type, {
            path: tubePath,
            radius: trunk.diameter / 2,
            sideOrientation: Mesh.DOUBLESIDE,
            updatable: true,
        }, SceneInstance.scene);
        return tubeTrunk;
    }

    private static splitCylinderUVFacesForTextures(): Vector4[] {
        const faceUV = [];
        // 0 - bottom base
        // 1 - between top and bottom
        // 2 - top base
        faceUV[0] = new Vector4(0, 0, 0.5, 1);
        faceUV[1] = new Vector4(0.5, 0, 1, 1);
        faceUV[2] = new Vector4(0, 0, 0.5, 1);

        return faceUV;
    }
}