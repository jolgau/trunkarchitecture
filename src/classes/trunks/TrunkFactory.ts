import { TrunkProperties } from "./properties/TrunkProperties";
import { BirchTrunkMesh } from "./meshes/BirchTrunkMesh";
import { PineTrunkMesh } from "./meshes/PineTrunkMesh";
import { MapleTrunkMesh } from "./meshes/MapleTrunkMesh";
import { BambooTrunkMesh } from "./meshes/BambooTrunkMesh";
import { Scene } from "@babylonjs/core/scene";
import { Mesh } from "@babylonjs/core/Meshes/mesh";
import { TrunksEnum } from "./utils/TrunksEnum";
import { SceneInstance } from "../utils/SceneInstance";

export class TrunkFactory {

    public getTrunk(type: TrunksEnum, trunkProperties: TrunkProperties): Mesh {
        switch (type) {
            case TrunksEnum.BIRCH:
                return BirchTrunkMesh.draw(trunkProperties);
            case TrunksEnum.PINE:
                return PineTrunkMesh.draw(trunkProperties);
            case TrunksEnum.MAPLE:
                return MapleTrunkMesh.draw(trunkProperties);
            case TrunksEnum.BAMBOO:
                return BambooTrunkMesh.draw(trunkProperties);
            default:
                throw new Error(`Trunk ${type} does not exists.`);
        }
    }
}