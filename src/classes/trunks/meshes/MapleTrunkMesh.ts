import { TextureBuilder } from "../../utils/TextureBuilder";
import { TrunkProperties } from "../properties/TrunkProperties";
import { TrunkDrawer } from "../utils/TrunkDrawer";
import { Scene } from "@babylonjs/core/scene";
import { Mesh } from "@babylonjs/core/Meshes/mesh";

export class MapleTrunkMesh {

    private static materialName: string = "mapleTrunkMaterial";
    
    private static diffuseMap: string = require("../../../assets/textures/trunks/MapleTrunk_DiffuseMap.jpg");
    private static bumpMap: string = require("../../../assets/textures/trunks/MapleTrunk_BumpMap.jpg");

    public static draw(trunk: TrunkProperties): Mesh {
        const trunkMesh = TrunkDrawer.drawCylindricalTrunk(trunk);
        
        const textureBuilder: TextureBuilder = new TextureBuilder(this.materialName);
        trunkMesh.material = textureBuilder.addDiffuseTexture(this.diffuseMap)
            .addBumpTexture(this.bumpMap)
            .build();
        return trunkMesh;
    }
}