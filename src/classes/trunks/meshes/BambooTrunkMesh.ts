import { TextureBuilder } from "../../utils/TextureBuilder";
import { TrunkProperties } from "../properties/TrunkProperties";
import { TrunkDrawer } from "../utils/TrunkDrawer";
import { Mesh } from "@babylonjs/core/Meshes/mesh";

export class BambooTrunkMesh {

    private static materialName: string = "pineTrunkMaterial";

    private static diffuseMap: string = require("../../../assets/textures/trunks/BambooTrunk_DiffuseMap.jpg");
    private static bumpMap: string = require("../../../assets/textures/trunks/BambooTrunk_BumpMap.jpg");

    public static draw(trunk: TrunkProperties): Mesh {
        const trunkMesh = TrunkDrawer.drawTubeTrunk(trunk);

        const textureBuilder: TextureBuilder = new TextureBuilder(this.materialName);
        trunkMesh.material = textureBuilder.addDiffuseTexture(this.diffuseMap)
            .addBumpTexture(this.bumpMap)
            .build();
        return trunkMesh;
    }
}