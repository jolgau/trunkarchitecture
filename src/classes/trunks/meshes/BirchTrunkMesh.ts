import { TextureBuilder } from "../../utils/TextureBuilder";
import { TrunkProperties } from "../properties/TrunkProperties";
import { TrunkDrawer } from "../utils/TrunkDrawer";
import { Scene } from "@babylonjs/core/scene";
import { Mesh } from "@babylonjs/core/Meshes/mesh";

export class BirchTrunkMesh {

    private static materialName: string = "birchTrunkMaterial";

    private static diffuseMap: string = require("../../../assets/textures/trunks/BirchTrunk_DiffuseMap.jpg");
    private static bumpMap: string = require("../../../assets/textures/trunks/BirchTrunk_BumpMap.jpg");

    public static draw(trunk: TrunkProperties): Mesh {
        const trunkMesh = TrunkDrawer.drawCylindricalTrunk(trunk);

        const textureBuilder: TextureBuilder = new TextureBuilder(this.materialName);
        trunkMesh.material = textureBuilder.addDiffuseTexture(this.diffuseMap)
            .addBumpTexture(this.bumpMap)
            .build();
        return trunkMesh;
    }
}