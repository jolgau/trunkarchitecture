import { Engine } from "@babylonjs/core/Engines/engine";
import { Scene } from "@babylonjs/core/scene";

export class SceneInstance {

    private static instance: Scene;

    private constructor() { }

    public static get scene(): Scene {
        if (!this.instance) {
            throw new Error("Engine was not set for the scene. Please set the engine.");
        }
        return this.instance;
    }

    public static init(engine: Engine) {
        this.instance = new Scene(engine);
    }
}