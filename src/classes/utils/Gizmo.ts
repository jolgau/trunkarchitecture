import { GizmoManager } from "@babylonjs/core/Gizmos/gizmoManager";
import { SceneInstance } from "./SceneInstance";

enum GizmoShortcuts {
    PositionShortcut = 'w',
    RotateShortcut = 'e',
    ScaleShortcut = 'r',
    BoundingBoxShortcut = 'q',
    HideGizmoShortcut = 'y',
    ToggleLocalGlobalGizmoPositionRotationShortcut = 'a',
    ToggleDistanceSnappingShortcut = 's',
    ToggleGizmoSizeShortcut = 'd'
}

export class Gizmo {

    private gizmoManager: GizmoManager;

    constructor() {
        this.gizmoManager = new GizmoManager(SceneInstance.scene);
        this.gizmoManager.positionGizmoEnabled = true;
        document.addEventListener('keydown', e => { this.triggerGizmoShortcuts(e.key) });
    }

    private triggerGizmoShortcuts(keyPressed: string): void {
        switch (keyPressed) {
            case GizmoShortcuts.PositionShortcut:
                this.disableGizmo();
                this.gizmoManager.positionGizmoEnabled = true;
                break;
            case GizmoShortcuts.RotateShortcut:
                this.disableGizmo();
                this.gizmoManager.rotationGizmoEnabled = true;
                break;
            case GizmoShortcuts.ScaleShortcut:
                this.disableGizmo();
                this.gizmoManager.scaleGizmoEnabled = true;
                break;
            case GizmoShortcuts.BoundingBoxShortcut:
                this.disableGizmo();
                this.gizmoManager.boundingBoxGizmoEnabled = true;
                break;
            case GizmoShortcuts.HideGizmoShortcut:
                this.gizmoManager.attachToMesh(null);
                break;
            case GizmoShortcuts.ToggleLocalGlobalGizmoPositionRotationShortcut:
                this.toggleLocalGlobalGizmoPositionRotation();
                break;
            case GizmoShortcuts.ToggleDistanceSnappingShortcut:
                this.toggleDistanceSnap()
                break;
            case GizmoShortcuts.ToggleGizmoSizeShortcut:
                this.toggleGizmoSize();
                break;
            default:
        }
    }

    private toggleGizmoSize(): void {
        if (this.gizmoManager.gizmos.scaleGizmo.scaleRatio == 1) {
            this.gizmoManager.gizmos.scaleGizmo.scaleRatio = 1.5;
            this.gizmoManager.gizmos.rotationGizmo.scaleRatio = 1.5;
            this.gizmoManager.gizmos.positionGizmo.scaleRatio = 1.5;
        } else {
            this.gizmoManager.gizmos.scaleGizmo.scaleRatio = 1;
            this.gizmoManager.gizmos.rotationGizmo.scaleRatio = 1;
            this.gizmoManager.gizmos.positionGizmo.scaleRatio = 1;
        }
    }

    private toggleDistanceSnap(): void {
        if (this.gizmoManager.gizmos.scaleGizmo.snapDistance == 0) {
            this.gizmoManager.gizmos.scaleGizmo.snapDistance = 0.3;
            this.gizmoManager.gizmos.rotationGizmo.snapDistance = 0.3;
            this.gizmoManager.gizmos.positionGizmo.snapDistance = 0.3;
        } else {
            this.gizmoManager.gizmos.scaleGizmo.snapDistance = 0;
            this.gizmoManager.gizmos.rotationGizmo.snapDistance = 0;
            this.gizmoManager.gizmos.positionGizmo.snapDistance = 0;
        }
    }

    private toggleLocalGlobalGizmoPositionRotation(): void {
        this.gizmoManager.gizmos.positionGizmo.updateGizmoRotationToMatchAttachedMesh = !this.gizmoManager.gizmos.positionGizmo.updateGizmoRotationToMatchAttachedMesh;
        this.gizmoManager.gizmos.rotationGizmo.updateGizmoRotationToMatchAttachedMesh = !this.gizmoManager.gizmos.rotationGizmo.updateGizmoRotationToMatchAttachedMesh;
    }

    private disableGizmo(): void {
        this.gizmoManager.positionGizmoEnabled = false;
        this.gizmoManager.rotationGizmoEnabled = false;
        this.gizmoManager.scaleGizmoEnabled = false;
        this.gizmoManager.boundingBoxGizmoEnabled = false;
    }
}