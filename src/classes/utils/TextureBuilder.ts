import { Scene } from "@babylonjs/core/scene";
import { StandardMaterial } from "@babylonjs/core/Materials/standardMaterial";
import { Texture } from "@babylonjs/core/Materials/Textures/texture";
import { SceneInstance } from "./SceneInstance";

// ideea e ca nu folosesc deloc eficient memoria daca la fiecare obiect folosesc un TextureBuilder...oare?
export class TextureBuilder {

    private scene: Scene;
    private material: StandardMaterial;

    constructor(name: string) {
        this.scene = SceneInstance.scene;
        this.material = new StandardMaterial(name, this.scene);
    }

    addDiffuseTexture(diffuseTexture: string): TextureBuilder {
        this.material.diffuseTexture = new Texture(diffuseTexture, this.scene);
        return this;
    }

    addBumpTexture(bumpTexture: string): TextureBuilder {
        this.material.bumpTexture = new Texture(bumpTexture, this.scene);
        return this;
    }

    build(): StandardMaterial {
        return this.material;
    }
}