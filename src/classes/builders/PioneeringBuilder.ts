import { TrunkProperties } from "../trunks/properties/TrunkProperties";
import { TrunkFactory } from "../trunks/TrunkFactory";
import { TrunksEnum } from "../trunks/utils/TrunksEnum";
import { Scene } from "@babylonjs/core/scene";
import { Mesh } from "@babylonjs/core/Meshes/mesh";
import { LightComposite } from "../environments/composites/LightComposite";

export class PioneeringBuilder {

    trunks: Mesh[] = [];
    trunkFactory: TrunkFactory;

    constructor() {
        this.trunkFactory = new TrunkFactory();
    }

    public createTrunk(): void {
        const trunkProperties: TrunkProperties = new TrunkProperties('pine', 80, 10, 60);
        const trunk = this.trunkFactory.getTrunk(TrunksEnum.PINE, trunkProperties);
        this.trunks.push(trunk);
    }
}