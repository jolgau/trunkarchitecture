import { AdvancedDynamicTexture } from '@babylonjs/gui/2D';
import { TopBarGUI } from './TopBarGUI';

export class AppGUI {

    private advancedTexture: AdvancedDynamicTexture;

    constructor() {
        this.advancedTexture = AdvancedDynamicTexture.CreateFullscreenUI("UI"); // .CreateFullscreenUI("UI");
        new TopBarGUI(this.advancedTexture);
    }

    // private addButton() {
    //     const cameraButton = require("../../assets/gui/CameraButton.png");
    //     const image =new Image("but", cameraButton);
    //     image.stretch = Image.STRETCH_UNIFORM;
    //     image.width = 0.1;
    //     image.height = 0.1;
    //     image.shadowColor = "black";
    //     image.shadowBlur = 5;
    //     image.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_LEFT;
    //     image.verticalAlignment = Control.VERTICAL_ALIGNMENT_TOP;
    //     // image.margin = 10;
    //     this.advancedTexture.addControl(image);
    //     /* var button = Button.CreateImageOnlyButton("but", cameraButton);
    //     button.width = 0.15;
    //     button.cornerRadius = 100;
    //     button.height = 0.15;
    //      this.advancedTexture.addControl(button);*/
    // }
}