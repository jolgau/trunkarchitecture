import { AdvancedDynamicTexture, Control, Image, Rectangle } from "@babylonjs/gui/2D";

export class TopBarGUI {

    private gizmoContainer: Rectangle;

    constructor(screen: AdvancedDynamicTexture) {
        this.setGizmoContainer();
        screen.addControl(this.gizmoContainer);

        /* Tools for GIZMO */
        // this.addHandTool();
        this.addMoveTool();
        // this.addRotateTool();
        // this.addScaleTool();
    }

    private addMoveTool(): void {
        // TODO: to implement
    }

    private setGizmoContainer(): void {
        this.gizmoContainer = new Rectangle();
        this.gizmoContainer.adaptHeightToChildren = true;
        this.gizmoContainer.adaptWidthToChildren = true;
        this.gizmoContainer.height = "100px";
        this.gizmoContainer.color = "transparent";
        // this.gizmoContainer.background = "green";

        this.gizmoContainer.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_CENTER;
        this.gizmoContainer.verticalAlignment = Control.VERTICAL_ALIGNMENT_TOP;

        this.addGizmoContainerBackgroundImage();

    }

    private addGizmoContainerBackgroundImage(): void {
        // Add Container Image
        const gizmoContainerImageLocation = require("../../assets/gui/topbar/GizmoContainer.png");
        const gizmoContainerImage = new Image("gizmoContainer", gizmoContainerImageLocation);
        gizmoContainerImage.stretch = Image.STRETCH_UNIFORM;
        gizmoContainerImage.height = 0.6;
        gizmoContainerImage.width = 0.6;
        gizmoContainerImage.paddingTop = -10;
        gizmoContainerImage.shadowColor = "black";
        gizmoContainerImage.shadowBlur = 5;
        gizmoContainerImage.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_CENTER;
        gizmoContainerImage.verticalAlignment = Control.VERTICAL_ALIGNMENT_TOP;
        this.gizmoContainer.addControl(gizmoContainerImage);
    }
}