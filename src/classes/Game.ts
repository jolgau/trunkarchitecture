import { Gizmo } from "./utils/Gizmo";
import { PioneeringBuilder } from "./builders/PioneeringBuilder";
import { GroundProperties } from "./environments/properties/GroundProperties";
import { GrassProperties } from "./environments/properties/GrassProperties";
import { Engine } from "@babylonjs/core/Engines/engine";
import { Scene } from "@babylonjs/core/scene";
import { Camera } from "@babylonjs/core/Cameras/camera";
import { ArcRotateCamera } from "@babylonjs/core/Cameras/arcRotateCamera";
import { Vector3, Color3 } from "@babylonjs/core/Maths/math";
import { Environment } from "./environments/Environment";
import { SceneInstance } from "./utils/SceneInstance";
import { AppGUI } from "./gui/AppGUI";
import { AdvancedDynamicTexture } from "@babylonjs/gui/2D/advancedDynamicTexture";
export class Game {

    private canvas: any;
    private engine: Engine;
    private scene: Scene;
    private camera: Camera;

    constructor(canvasId: string) {
        this.canvas = document.getElementById(canvasId);
    }

    public initialize(): void {
        this.engine = new Engine(this.canvas, true);

        this.addScene();
        this.addCamera();
        this.addEnvironment();
        this.addGUI();
        this.addUtils();
        this.addPresetObjects();

        this.engine.runRenderLoop(() => {
            this.scene.render();
        });
    }

    private addScene(): void {
        SceneInstance.init(this.engine);
        this.scene = SceneInstance.scene;
    }

    private addCamera(): void {
        this.camera = new ArcRotateCamera(
            "camera",
            Math.PI / 2,
            Math.PI / 3,
            35,
            Vector3.Zero(),
            this.scene
        );
        this.camera.attachControl(this.engine.getRenderingCanvas(), true);
    }

    private addEnvironment(): void {
        const environment: Environment = new Environment();

        const groundProperties: GroundProperties = new GroundProperties("ground", 10, 60, 80);
        const grassProperties: GrassProperties = new GrassProperties("grass", 200, false, 3, new Color3(0.54, 0.89, 0.2));

        environment.addGround(groundProperties);
        environment.groundComposite.addGrass(grassProperties);
    }

    private addGUI() {
        const gui: AppGUI = new AppGUI();
    }

    private addPresetObjects(): void {
        const pioneeringBuilder = new PioneeringBuilder();
        pioneeringBuilder.createTrunk();
    }

    private addUtils(): void {
        new Gizmo();
        window.addEventListener("resize", this.onResizeWindow);
    }

    private onResizeWindow = () => {
        if (this.engine) {
            this.engine.resize();
        }
    }
}