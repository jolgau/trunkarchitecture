import { GroundComposite } from "./composites/GroundComposite";
import { GroundProperties } from "./properties/GroundProperties";
import { Scene } from "@babylonjs/core/scene";
import { SkyboxMesh } from "./meshes/SkyboxMesh";
import { PlaneMesh } from "./meshes/PlaneMesh";
import { PlaneProperties } from "./properties/PlaneProperties";
import { SkyboxProperties } from "./properties/SkyboxProperties";
import { LightComposite } from "./composites/LightComposite";
import { SceneInstance } from "../utils/SceneInstance";

export class Environment {

    private ground: GroundComposite;
    private lights: LightComposite;

    private mapSize: number = 2000;

    constructor() {
        SkyboxMesh.draw(new SkyboxProperties("skybox", this.mapSize));
        PlaneMesh.draw(new PlaneProperties("plane", this.mapSize, this.mapSize));
        this.initLights();
    }

    public get groundComposite(): GroundComposite {
        return this.ground;
    }

    public addGround(groundProperties: GroundProperties): void {
        this.ground = new GroundComposite(groundProperties);
        // this.lights.addMeshToCashShadow(this.groundComposite.ground);
    }

    public removeGround(): void {
        this.groundComposite.removeGround();
    }

    private initLights() {
        this.lights = LightComposite.instance;
        this.lights.init(this.mapSize);
        this.lights.addLights();
        // this.lights.enableShadow();
    }
}