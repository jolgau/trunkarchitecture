import { TextureBuilder } from "../../utils/TextureBuilder";
import { GroundProperties } from "../properties/GroundProperties";
import { Mesh } from "@babylonjs/core/Meshes/mesh";
import { MeshBuilder } from "@babylonjs/core/Meshes/meshBuilder";
import { SceneInstance } from "../../utils/SceneInstance";

export class GroundMesh {

    private static materialName: string = "groundMaterial";
    private static diffuseMap: string = require("../../../assets/textures/ground/soul_layer_texture_2.jpg");

    public static draw(ground: GroundProperties): Mesh {
        const textureBuilder: TextureBuilder = new TextureBuilder(this.materialName);

        const groundMesh = MeshBuilder.CreateBox(ground.name, {
            height: ground.height,
            width: ground.width,
            depth: ground.depth
        }, SceneInstance.scene);

        groundMesh.material = textureBuilder.addDiffuseTexture(this.diffuseMap).build();
        return groundMesh;
    }
}