import { Mesh } from "@babylonjs/core/Meshes/mesh";
import { CubeTexture } from "@babylonjs/core/Materials/Textures/cubeTexture";
import { StandardMaterial } from "@babylonjs/core/Materials/standardMaterial";
import { Texture } from "@babylonjs/core/Materials/Textures/texture";
import { SkyboxProperties } from "../properties/SkyboxProperties";
import { SceneInstance } from "../../utils/SceneInstance";

export class SkyboxMesh {

    private static skyboxStandardMaterialName = "skybox";
    private static skyboxMaterialName = "textures/sky/TropicalSunnyDay";

    public static draw(skybox: SkyboxProperties): Mesh {

        const skyboxMesh = Mesh.CreateBox(skybox.name, skybox.size, SceneInstance.scene);
        const skyboxMaterial = new StandardMaterial(this.skyboxStandardMaterialName, SceneInstance.scene);
        skyboxMaterial.backFaceCulling = false;
        skyboxMaterial.disableLighting = true;

        skyboxMesh.isPickable = false;
        skyboxMesh.material = skyboxMaterial;
        skyboxMesh.infiniteDistance = true;
        skyboxMesh.renderingGroupId = 0;

        skyboxMaterial.reflectionTexture = new CubeTexture(this.skyboxMaterialName, SceneInstance.scene)
        skyboxMaterial.reflectionTexture.coordinatesMode = Texture.SKYBOX_MODE;

        return skyboxMesh;
    }
}