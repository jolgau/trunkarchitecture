import { GrassProperties } from "../properties/GrassProperties";
import { Vector3 } from "@babylonjs/core/Maths/math";
import { Mesh } from "@babylonjs/core/Meshes/mesh";
import { SceneInstance } from "../../utils/SceneInstance"
import { FurMaterial } from "@babylonjs/materials/fur/furMaterial";

export class GrassMesh {

    private static furMaterialName: string = "furMaterial";

    public static draw(grass: GrassProperties, width: number, height: number, position: Vector3): Mesh {
        const groundGrass = Mesh.CreateGround(grass.name, width, height, grass.subdivides, SceneInstance.scene, true)
        groundGrass.position = position;
        groundGrass.receiveShadows = true;
        
        const furMaterial = new FurMaterial(this.furMaterialName, SceneInstance.scene);
        furMaterial.highLevelFur = grass.highLevelFur;
        furMaterial.furLength = grass.furLength;
        furMaterial.furColor = grass.furColor
        groundGrass.material = furMaterial;

        return groundGrass;
    }
}