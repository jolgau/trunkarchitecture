import { Scene } from "@babylonjs/core/scene";
import { Mesh } from "@babylonjs/core/Meshes/mesh";
import { MeshBuilder } from "@babylonjs/core/Meshes/meshBuilder";
import { Plane, Color3 } from "@babylonjs/core/Maths/math";
import { PlaneProperties } from "../properties/PlaneProperties";
import { StandardMaterial } from "@babylonjs/core/Materials/standardMaterial";
import { SceneInstance } from "../../utils/SceneInstance";

export class PlaneMesh {

    public static draw(plane: PlaneProperties): Mesh {
        const sourcePlane = new Plane(0, -1, 0, 0);
        sourcePlane.normalize();
        const planeMesh = MeshBuilder.CreatePlane(plane.name, {
            height: plane.height,
            width: plane.width,
            sourcePlane: sourcePlane,
            sideOrientation: Mesh.DOUBLESIDE
        }, SceneInstance.scene);

        const material = new StandardMaterial("noSpeculaColorMaterial", SceneInstance.scene);
        material.diffuseColor = new Color3(230, 230, 230);
        material.specularColor = new Color3(0, 0, 0);
        planeMesh.material = material;

        planeMesh.isPickable = false;
        planeMesh.renderingGroupId = 0;
        planeMesh.receiveShadows = true;
        return planeMesh;
    }
}