import { GrassProperties } from "../properties/GrassProperties";
import { GroundProperties } from "../properties/GroundProperties";
import { GrassMesh } from "../meshes/GrassMesh";
import { GroundMesh } from "../meshes/GroundMesh";
import { Mesh } from "@babylonjs/core/Meshes/mesh";
import { Vector3 } from "@babylonjs/core//Maths/math";

export class GroundComposite {

    ground: Mesh;
    grass: Mesh;

    constructor(ground: GroundProperties) {
        this.ground = GroundMesh.draw(ground);
    }

    public addGrass(grassProperties: GrassProperties): void {
        // Positionate grass on top of the ground mesh
        const grassPosition: Vector3 = new Vector3(this.ground.position.x, this.ground.position.y, this.ground.position.z);
        const vectorsWorld = this.ground.getBoundingInfo().boundingBox.vectors;
        let width = Number(vectorsWorld[1].x - (vectorsWorld[0].x))
        let height = Number(vectorsWorld[1].y - (vectorsWorld[0].y))
        let depth = Number(vectorsWorld[1].z - (vectorsWorld[0].z))
        grassPosition.y = this.ground.position.y + (height / 2) - 1;

        this.grass = GrassMesh.draw(grassProperties, width, depth, grassPosition)
        this.ground.addChild(this.grass);
    }

    public removeGrass() {
        this.grass.dispose();
    }

    public removeGround() {
        this.ground.dispose();
    }
}