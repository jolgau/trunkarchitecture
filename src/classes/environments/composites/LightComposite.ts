import { IShadowLight } from "@babylonjs/core/Lights/shadowLight";
import { ShadowGenerator } from "@babylonjs/core/Lights/Shadows/shadowGenerator";
import { ShadowGeneratorSceneComponent } from "@babylonjs/core/Lights/Shadows/shadowGeneratorSceneComponent";
import { DirectionalLight } from "@babylonjs/core/Lights/directionalLight";
import { Vector3 } from "@babylonjs/core/Maths/math";
import { AbstractMesh } from "@babylonjs/core/Meshes/abstractMesh";
import { SceneInstance } from "../../utils/SceneInstance";
import { PointLight } from "@babylonjs/core/Lights/pointLight";
 
// Singleton Design Pattern
export class LightComposite {

    private static _instance: LightComposite;

    private sunLight: IShadowLight;
    private shadowGenerator: ShadowGenerator;
    private mapSize: number;

    public static get instance() {
        if (!this._instance) {
            this._instance = new LightComposite();
        }
        return this._instance;
    }

    private constructor() { }

    public init(mapSize: number): void {
        this.mapSize = mapSize;
    }
    
    public enableShadow(): void {
        new ShadowGeneratorSceneComponent(SceneInstance.scene);
        this.shadowGenerator = new ShadowGenerator(this.mapSize, this.sunLight);
        this.shadowGenerator.usePercentageCloserFiltering = true;
        this.shadowGenerator.filteringQuality = ShadowGenerator.QUALITY_HIGH;
    }

    public disableShadow(): void {
        this.shadowGenerator.dispose();
    }

    public addLights(): void {
        new PointLight("PointLight", new Vector3(-100, 100, -100), SceneInstance.scene);
        this.sunLight = new DirectionalLight("DirectionalLight", new Vector3(-100, -100, -100), SceneInstance.scene);
    }

    public addMeshToCashShadow(mesh: AbstractMesh): void {
        this.shadowGenerator.getShadowMap().renderList.push(mesh);
    }
}