import { Color3 } from "@babylonjs/core/Maths/math";

export class GrassProperties {

    private _name: string;
    private _subdivides: number;
    private _highLevelFur: boolean;
    private _furLength: number;
    private _furColor: Color3;

    constructor(name: string, subdivides: number, highLevelFur: boolean, furLength: number, furColor: Color3) {
        this._name = name;
        this._subdivides = subdivides;
        this._highLevelFur = highLevelFur;
        this._furLength = furLength;
        this._furColor = furColor;
    }

    public get name(): string {
        return this._name;
    }

    public set name(value: string) {
        this._name = value;
    }

    public get subdivides(): number {
        return this._subdivides;
    }

    public set subdivides(value: number) {
        this._subdivides = value;
    }

    public get highLevelFur(): boolean {
        return this._highLevelFur;
    }

    public set highLevelFur(value: boolean) {
        this._highLevelFur = value;
    }

    public get furLength(): number {
        return this._furLength;
    }

    public set furLength(value: number) {
        this._furLength = value;
    }

    public get furColor(): Color3 {
        return this._furColor;
    }

    public set furColor(value: Color3) {
        this._furColor = value;
    }

}