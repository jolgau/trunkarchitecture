export class GroundProperties {

    private _name: string;
    private _height: number;
    private _width: number;
    private _depth: number;

    constructor(name: string, height: number, width: number, depth: number) {
        this._name = name;
        this._height = height;
        this._width = width;
        this._depth = depth;
    }

    public get name(): string {
        return this._name;
    }

    public set name(value: string) {
        this._name = value;
    }

    public get height(): number {
        return this._height;
    }

    public set height(value: number) {
        this._height = value;
    }

    public get width(): number {
        return this._width;
    }

    public set width(value: number) {
        this._width = value;
    }

    public get depth(): number {
        return this._depth;
    }

    public set depth(value: number) {
        this._depth = value;
    }
}