export class PlaneProperties {

    private _name: string;
    private _width: number;
    private _height: number;

    constructor(name: string, width: number, height: number) {
        this._name = name;
        this._width = width;
        this._height = height;
    }

    public get name(): string {
        return this._name;
    }
    public set name(value: string) {
        this._name = value;
    }
    public get width(): number {
        return this._width;
    }
    public set width(value: number) {
        this._width = value;
    }
    public get height(): number {
        return this._height;
    }
    public set height(value: number) {
        this._height = value;
    }
}