const path = require('path');

module.exports = {
    entry: './src/PioneeringApp.ts',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'app.bundle.js'
    },
    resolve: {
        extensions: ['.ts', '.js']
    },
    module: {
        rules: [{
            test: /\.ts?$/,
            exclude: '/node_modules/',
            loader: 'babel-loader',
            query: {
                presets: [
                    "@babel/env",
                    "@babel/preset-typescript"
                ],
                plugins: [
                    "@babel/proposal-class-properties",
                    "@babel/proposal-object-rest-spread"
                ]
            }
        }, {
            test: /\.(jpg|png)$/,
            loader: 'url-loader?name=/app/assets/[name].[ext]'
        }]
    }
}